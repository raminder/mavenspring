package com.learn.mavenspring.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.learn.mavenspring.models.onecard.User;
import com.learn.mavenspring.models.onecard.UserDao;

public class UserMapper implements RowMapper<User> {

	public User mapRow(ResultSet resultSet, int rowNum) throws SQLException {

		String firstName = resultSet.getString(UserDao.COL_FIRST_NAME);
		String lastName = resultSet.getString(UserDao.COL_LAST_NAME);
		String email = resultSet.getString(UserDao.COL_EMAIL);
		int gender = resultSet.getInt(UserDao.COL_GENDER);
		String password = resultSet.getString(UserDao.COL_PASSWORD);
		String phoneNumber = resultSet.getString(UserDao.COL_PHONE_NUM);
		String mpin = resultSet.getString(UserDao.COL_MPIN);
		int id = resultSet.getInt(UserDao.COL_ID);
		User user = new User(firstName, lastName, email, phoneNumber, password, gender);
		user.set_userId(id);
		user.set_mpin(mpin);
		return user;
	}

}
