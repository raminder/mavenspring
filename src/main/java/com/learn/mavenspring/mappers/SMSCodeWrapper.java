package com.learn.mavenspring.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.springframework.jdbc.core.RowMapper;

import com.learn.mavenspring.models.onecard.SMSCodeTuple;
import com.learn.mavenspring.models.onecard.SMSCodesDao;

public class SMSCodeWrapper implements RowMapper<SMSCodeTuple> {

	public SMSCodeTuple mapRow(ResultSet resultSet, int arg1) throws SQLException {
		Integer id = resultSet.getInt(SMSCodesDao.COL_ID);
		Timestamp timestamp = resultSet.getTimestamp(SMSCodesDao.COL_TIMESTAMP);
		String code = resultSet.getString(SMSCodesDao.COL_SMS_CODE);
		String number = resultSet.getString(SMSCodesDao.COL_MOBILE_NUM);
		SMSCodeTuple tuple = new SMSCodeTuple();
		tuple.setCode(code);
		tuple.setId(id);
		tuple.setTimestamp(timestamp.getTime());
		tuple.setMobileNumber(number);
		return tuple;
	}
}
