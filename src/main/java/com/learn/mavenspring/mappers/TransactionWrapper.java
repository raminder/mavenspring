package com.learn.mavenspring.mappers;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.learn.mavenspring.models.onecard.Transaction;
import com.learn.mavenspring.models.onecard.TransactionDao;
import com.learn.mavenspring.models.onecard.TranxType;

public class TransactionWrapper implements RowMapper<Transaction> {

	public Transaction mapRow(ResultSet rs, int rowNum) throws SQLException {

		String mobileNumber = rs.getString(TransactionDao.COL_TO_MOBILE_NUM);
		String tranxId = rs.getString(TransactionDao.COL_TRANX_ID);
		String type = rs.getString(TransactionDao.COL_TYPE);
		Double amount = rs.getDouble(TransactionDao.COL_AMOUNT);
		Integer userId = rs.getInt(TransactionDao.COL_ID);
		String cardId = rs.getString(TransactionDao.COL_FROM_CARD_ID);
		Date timestamp = rs.getDate(TransactionDao.COL_TIMESTAMP);
		String description = rs.getString(TransactionDao.COL_DESCRIPTION);
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setMobileNumber(mobileNumber);
		transaction.setTranxId(tranxId);
		transaction.setUserId(userId);
		transaction.setType(TranxType.getEnum(type));
		transaction.setCardId(cardId);
		transaction.setTimestamp(timestamp.getTime());
		transaction.setDescription(description);
		return transaction;
	}

}
