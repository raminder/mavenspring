package com.learn.mavenspring.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.learn.mavenspring.models.onecard.Card;
import com.learn.mavenspring.models.onecard.CardDao;

public class CardWrapper implements RowMapper<Card> {

	public Card mapRow(ResultSet rs, int rowNum) throws SQLException {

		Double amount = rs.getDouble(CardDao.COL_AMOUNT);
		String cardId = rs.getString(CardDao.COL_CARD_ID);
		return new Card(cardId, amount);
	}

}
