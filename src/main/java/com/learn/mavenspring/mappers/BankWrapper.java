package com.learn.mavenspring.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.learn.mavenspring.models.onecard.Bank;
import com.learn.mavenspring.models.onecard.BankDao;

public class BankWrapper implements RowMapper<Bank> {

	public Bank mapRow(ResultSet rs, int rowNum) throws SQLException {

		String orgId = rs.getString(BankDao.COL_ORD_ID);
		String name = rs.getString(BankDao.COL_NAME);
		Bank bank = new Bank();
		bank.setOrgId(orgId);
		bank.setName(name);
		bank.setHandle(rs.getString(BankDao.COL_HANDLE));
		bank.setIfsc(rs.getString(BankDao.COL_IFSC));
		bank.setIin(rs.getString(BankDao.COL_IIN));
		bank.setNbin(rs.getString(BankDao.COL_NBIN));
		bank.setLogoUrl(rs.getString(BankDao.COL_LOGO_URL));
		return bank;
	}

}
