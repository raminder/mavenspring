package com.learn.mavenspring.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.learn.mavenspring.models.onecard.ReceiveFromDao;
import com.learn.mavenspring.models.onecard.ReceivePayment;

public class ReceivePaymentWrapper implements RowMapper<ReceivePayment> {

	public ReceivePayment mapRow(ResultSet rs, int rowNum) throws SQLException {

		Integer userId = rs.getInt(ReceiveFromDao.COL_ID);
		String userMobileNumber = rs.getString(ReceiveFromDao.COL_MOBILE_NUM);
		Integer fromUserId = rs.getInt(ReceiveFromDao.COL_FROM_USER_ID);
		String fromUserMobileNumber = rs.getString(ReceiveFromDao.COL_FROM_MOBILE_NUMBER);
		Double amount = rs.getDouble(ReceiveFromDao.COL_AMOUNT);
		String tranxId = rs.getString(ReceiveFromDao.COL_TRANX_ID);
		String toCardId = rs.getString(ReceiveFromDao.COL_IN_CARD_ID);
		String messageId = rs.getString(ReceiveFromDao.COL_GCM_MESSAGE_ID);

		ReceivePayment receivePayment = new ReceivePayment();
		receivePayment.setAmount(amount);
		receivePayment.setCardId(toCardId);
		receivePayment.setUserId(userId);
		receivePayment.setUserMobileNumber(userMobileNumber);
		receivePayment.setFromUserId(fromUserId);
		receivePayment.setFromMobileNumber(fromUserMobileNumber);
		receivePayment.setTranxId(tranxId);
		receivePayment.setGcmMessageId(messageId);
		return receivePayment;

	}

}
