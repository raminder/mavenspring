package com.learn.mavenspring.models.questions;

public interface QuestionsDBInterface {

	
	
	public int insertCategory(String categName);

	public int insertQuestion();

	public int insertAQuorator(String name);

}
