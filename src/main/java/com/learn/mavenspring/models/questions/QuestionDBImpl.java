package com.learn.mavenspring.models.questions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.learn.mavenspring.Utils;

public class QuestionDBImpl implements QuestionsDBInterface, DBConstants {

	private DataSource _dataSource;

	public void setDataSource(DataSource datasource) {
		this._dataSource = datasource;
	}

	public int insertCategory(String categName) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int insertQuestion() {
		String qText = "Who is the prime minister of India";
		String qa = "Manmohan Singh";
		String qb = "Raminder Singh";
		String qc = "Iqbal Singh";
		String qd = "Modi";

		String insertQuestionSql = "INSERT INTO "
				+ TABLE_QUESTIONS
				+ "(q_text,q_a,q_b,q_c,q_d,ans,qurator,qcategory,date_time)  VALUES (?,?,?,?,?,?,?,?,?)";

		Connection conn = null;

		try {
			conn = _dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(insertQuestionSql);
			ps.setString(1, qText);
			ps.setString(2, qa);
			ps.setString(3, qb);
			ps.setString(4, qc);
			ps.setString(5, qd);
			ps.setString(6, "a");
			ps.setString(7, "Raman");
			ps.setInt(8, 1);
			// The JDBC driver knows what to do with a java.sql type:
			ps.setObject(9, Utils.getCurrentDate());
			int rowsAffected = ps.executeUpdate();
			ps.close();
			return rowsAffected;
		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	public int insertAQuorator(String name) {
		// TODO Auto-generated method stub
		return 0;
	}

	public PreparedStatement runquery(DataSource source, String sql) throws SQLException {
			
		return source.getConnection().prepareStatement(sql);
	}
}
