package com.learn.mavenspring.models;

public class Status {

	private boolean status;

	public Status(boolean status) {
		this.status = status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean getStatus() {
		return status;
	}
}
