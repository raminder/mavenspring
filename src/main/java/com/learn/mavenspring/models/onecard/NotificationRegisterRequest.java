package com.learn.mavenspring.models.onecard;

public class NotificationRegisterRequest {

	private String userId;
	private String deviceId;
	private String gcmId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String device) {
		this.deviceId = device;
	}

	public String getGcmId() {
		return gcmId;
	}

	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}

}
