package com.learn.mavenspring.models.onecard;

import java.io.Serializable;
import java.sql.Date;

public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String _firstName;
	String _lastName;
	String _email;
	String _phoneNumber;
	String _password;
	Integer _gender; // 1 man,2 female
	Date _dob;
	Integer _userId;
	String _mpin;

	public User() {
	}

	public User(String firstName, String lastName, String email, String phoneNumber, String password, int gender) {
		this._firstName = firstName;
		this._lastName = lastName;
		this._email = email;
		this._phoneNumber = phoneNumber;
		this._password = password;
		this._gender = gender;
	}

	public String get_firstName() {
		return _firstName;
	}

	public String get_lastName() {
		return _lastName;
	}

	public String get_email() {
		return _email;
	}

	public String get_phoneNumber() {
		return _phoneNumber;
	}

	public String get_password() {
		return _password;
	}

	public int get_gender() {
		return _gender;
	}

	public void set_userId(int id) {
		this._userId = id;
	}

	public Integer get_userId() {
		return this._userId;
	}

	public Date get_dob() {
		return _dob;
	}

	public void set_dob(Date date) {
		this._dob = date;
	}

	public void set_mpin(String mpin) {
		this._mpin = mpin;
	}

	public String get_mpin() {
		return this._mpin;
	}
}
