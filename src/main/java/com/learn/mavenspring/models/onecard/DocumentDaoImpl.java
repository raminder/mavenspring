package com.learn.mavenspring.models.onecard;

import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

public class DocumentDaoImpl implements DocumentsDao {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		jdbcTemplate = new JdbcTemplate(dataSource);

	}

	public boolean save(Document doc) {
		if (!anyDocExists(doc.getUserId())) {

			return addFirstDoc(doc);
		} else {

			return updateUserDocs(doc);
		}
	}

	private boolean updateUserDocs(Document doc) {
		String updateSql = " UPDATE " + TABLE_NAME + " SET " + doc.getFileType().getValue() + " = ? " + " WHERE "
				+ COL_USER_ID + " = ?";
		System.out.println("update sql for doc" + updateSql);
		Object[] values = { doc.getFilePath(), doc.getUserId() };
		int[] types = { Types.VARCHAR, Types.INTEGER };
		int cnt = jdbcTemplate.update(updateSql, values, types);
		if (cnt == 0) {
			return false;
		} else {
			return true;
		}
	}

	private boolean addFirstDoc(Document doc) {
		String insertSql = " INSERT INTO " + TABLE_NAME + " ( " + doc.getFileType().getValue() + " , " + COL_USER_ID
				+ ") values(?,?)";
		System.out.println("save doc query " + insertSql);
		Object[] values = { doc.getFilePath(), doc.getUserId() };
		int[] types = { Types.VARCHAR, Types.INTEGER };
		int cnt = jdbcTemplate.update(insertSql, values, types);
		if (cnt == 0) {
			return false;
		} else {
			return true;
		}
	}

	public boolean anyDocExists(Integer userId) {
		String sql = " select count(*) from " + TABLE_NAME + " where " + COL_USER_ID + " = ? ";
		System.out.println("any doc exists query" + sql);
		int cnt = jdbcTemplate.queryForObject(sql, Integer.class, userId.toString());
		if (cnt == 0) {
			return false;
		} else {
			return true;
		}
	}

	public List<FileType> fetchAll(Integer userId) {
		return null;
	}

}
