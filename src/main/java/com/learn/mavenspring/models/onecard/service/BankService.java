package com.learn.mavenspring.models.onecard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learn.mavenspring.models.onecard.Bank;
import com.learn.mavenspring.models.onecard.BankDao;

@Service
public class BankService {

	@Autowired
	private BankDao _bankDao;

	public List<Bank> getRegisteredBankList() {
		return _bankDao.getRegisteredBanks();
	}

}
