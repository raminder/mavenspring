package com.learn.mavenspring.models.onecard;

import java.util.List;

import javax.sql.DataSource;

public interface TransactionDao {

	public static final String TABLE_NAME = "transactions";
	public static final String COL_ID = "user_id";
	public static final String COL_TO_MOBILE_NUM = "mobile_number";
	public static final String COL_FROM_CARD_ID = "card_id";
	public static final String COL_TRANX_ID = "tranx_id";
	public static final String COL_AMOUNT = "amount";
	public static final String COL_TYPE = "type";
	public static final String COL_TIMESTAMP = "timestamp";
	public static final String COL_DESCRIPTION = "description";

	void setDataSource(DataSource dataSource);

	public boolean pay(PayRequest payRequest, TranxType tranxType, String tranxId);

	public List<Transaction> getAll(Integer userId);

	public List<Transaction> getAll(Integer userId, String cardId);

}
