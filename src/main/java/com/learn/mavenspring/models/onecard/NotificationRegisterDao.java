package com.learn.mavenspring.models.onecard;

import javax.sql.DataSource;

public interface NotificationRegisterDao {

	public static final String TABLE_NAME = "notification_registry";
	public static final String COL_ID = "user_id";
	public static final String COL_GCM_REG_ID = "gcm_reg_id";
	public static final String COL_DEVICE_ID = "device_id";
	public static final String COL_SOURCE = "source";
	public static final String COL_CREATE_TIME = "create_time";
	public static final String COL_IS_ACTIVE = "is_Active";

	void setDataSource(DataSource dataSource);

	boolean register(NotificationRegisterRequest registerReg);

	boolean deregister(NotificationRegisterRequest deregisterReq);

	String getGcmId(Integer userId);

	void updateGcmId(Integer userId, String gcmId);

}
