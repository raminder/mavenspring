package com.learn.mavenspring.models.onecard;

import javax.sql.DataSource;

public interface UserDao {

	final String TABLE_NAME = "user";
	final String COL_FIRST_NAME = "first_name";
	final String COL_LAST_NAME = "last_name";
	final String COL_EMAIL = "email";
	final String COL_GENDER = "gender";
	final String COL_PASSWORD = "password";
	final String COL_PHONE_NUM = "mobile_number";
	final String COL_DOB = "dob";
	final String COL_ID = "id";
	final String COL_MPIN = "mpin";
	final String COL_NUM_VERIFIED = "number_verified";

	void setDataSource(DataSource dataSource);

	String create(User user);

	boolean exists(User user);

	Integer getId(String mobileNumber);

	User exists(String mobileNumber);

	public User exists(Integer userId);

	boolean isVerified(String mobileNumber);

	boolean markVerified(String mobileNumber);

	boolean creatMPIN(Integer userId, String mpin);

	boolean verifyMPIN(Integer userId, String mpin);
}
