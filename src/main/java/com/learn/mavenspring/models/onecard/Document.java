package com.learn.mavenspring.models.onecard;

public class Document {

	FileType fileType;
	Integer userId;
	String filePath;

	public Document() {

	}

	public Document(FileType fileType, Integer userId, String filePath) {
		super();
		this.fileType = fileType;
		this.userId = userId;
		this.filePath = filePath;
	}

	public FileType getFileType() {
		return fileType;
	}

	public Integer getUserId() {
		return userId;
	}

	public String getFilePath() {
		return filePath;
	}
	
	

}
