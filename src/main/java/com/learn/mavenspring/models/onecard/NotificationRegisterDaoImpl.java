package com.learn.mavenspring.models.onecard;

import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.learn.mavenspring.mappers.SMSCodeWrapper;

public class NotificationRegisterDaoImpl implements NotificationRegisterDao {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		jdbcTemplate = new JdbcTemplate(this.dataSource);

	}

	public boolean register(NotificationRegisterRequest registerReg) {

		String insertSql = "INSERT INTO " + TABLE_NAME + " ( " + COL_ID + " , " + COL_DEVICE_ID + " , " + COL_GCM_REG_ID
				+ " , " + COL_IS_ACTIVE + " ) values(?,?,?,?) ON DUPLICATE KEY update " + COL_GCM_REG_ID + " = ? , "
				+ COL_IS_ACTIVE + " = ? ";
		int[] types = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.TINYINT, Types.VARCHAR, Types.TINYINT };
		Object[] objects = { registerReg.getUserId(), registerReg.getDeviceId(), registerReg.getGcmId(), 1,
				registerReg.getGcmId(), 1 };
		int cnt = jdbcTemplate.update(insertSql, objects, types);
		if (cnt > 0) {
			return true;
		} else {
			return false;
		}

	}

	public boolean deregister(NotificationRegisterRequest deregisterReq) {

		String updateSql = "UPDATE " + TABLE_NAME + " SET " + COL_IS_ACTIVE + " = ?  WHERE " + COL_ID + " = ? and  "
				+ COL_GCM_REG_ID + " = ?";
		int[] types = { Types.TINYINT, Types.VARCHAR, Types.VARCHAR };
		Object[] objects = { 0, deregisterReq.getUserId(), deregisterReq.getGcmId() };
		int cnt = jdbcTemplate.update(updateSql, objects, types);
		if (cnt > 0) {
			return true;
		} else {
			return false;
		}
	}

	public String getGcmId(Integer userId) {
		try {
			String sql = "select " + COL_GCM_REG_ID + " from " + TABLE_NAME + " where " + COL_ID + " = ?";
			Object[] objects = { userId };
			int[] types = { Types.INTEGER };
			return jdbcTemplate.queryForObject(sql, objects, types, String.class);
		} catch (EmptyResultDataAccessException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			return null;
		}
	}

	public void updateGcmId(Integer userId, String gcmId) {
		String updateSql = "UPDATE " + TABLE_NAME + " SET " + COL_GCM_REG_ID + " = ? where " + COL_ID + " = ?";
		int[] types = { Types.VARCHAR, Types.INTEGER };
		Object[] objects = { gcmId, userId };
		jdbcTemplate.update(updateSql, objects, types);
	}

}
