package com.learn.mavenspring.models.onecard;

import javax.sql.DataSource;

public interface SMSCodesDao {

	String TABLE_NAME = "sms_codes";
	String COL_ID = "id";
	String COL_MOBILE_NUM = "mobile_number";
	String COL_SMS_CODE = "code";
	String COL_TIMESTAMP = "timestamp";

	void setDataSource(DataSource datasource);

	boolean addCode(Integer id, String mobileNumber, String smsCode);

	boolean updateCode(Integer id, String mobileNum, String smsCode);

	boolean remove(int id, String mobileNumber);

	boolean isCodePresent(int id, String mobileNumber);

	SMSCodeTuple getSMSTuple(String mobileNumber);
	/*
	 * CREATE TABLE `onecard`.`sms_codes` ( `id` INT NOT NULL, `mobile_number`
	 * VARCHAR(45) NOT NULL, `code` VARCHAR(6) NOT NULL, `timestamp` TIMESTAMP
	 * NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`));
	 */
}
