package com.learn.mavenspring.models.onecard;

public class Address {

	Integer id;
	String street;
	String city;
	String pincode;

	public Address()
	{
		
	}

	public Address(Integer id, String street, String city, String pincode) {
		this.id = id;
		this.street = street;
		this.city = city;
		this.pincode = pincode;
	}

	public Integer getId() {
		return id;
	}

	public String getStreet() {
		return street;
	}

	public String getCity() {
		return city;
	}

	public String getPincode() {
		return pincode;
	}
	
	
}
