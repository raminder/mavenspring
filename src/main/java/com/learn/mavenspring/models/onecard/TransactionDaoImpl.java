package com.learn.mavenspring.models.onecard;

import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.learn.mavenspring.mappers.TransactionWrapper;

public class TransactionDaoImpl implements TransactionDao {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		jdbcTemplate = new JdbcTemplate(dataSource);

	}

	public boolean pay(PayRequest payRequest, TranxType tranxType, String tranxId) {
		String sql = " INSERT INTO " + TABLE_NAME + " ( " + COL_ID + " , " + COL_TO_MOBILE_NUM + " , "
				+ COL_FROM_CARD_ID + " , " + COL_AMOUNT + " , " + COL_TYPE + " , " + COL_TRANX_ID + " , "
				+ COL_DESCRIPTION + ") values (?,?,?,?,?,?,?)";
		Object[] objects = { payRequest.get_userId(), payRequest.get_toMobileNumber(), payRequest.get_cardId(),
				payRequest.get_amount(), tranxType.getValue(), tranxId, payRequest.get_description() };
		int[] types = { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR };
		int cnt = jdbcTemplate.update(sql, objects, types);
		if (cnt > 0) {
			return true;
		} else {
			return false;
		}
	}

	public List<Transaction> getAll(Integer userId) {

		String sql = "SELECT * from " + TABLE_NAME + " where " + COL_ID + " = ?";
		Object[] objects = { userId };
		int[] types = { Types.INTEGER };
		return jdbcTemplate.query(sql, objects, types, new TransactionWrapper());
	}

	public List<Transaction> getAll(Integer userId, String cardId) {
		String sql = "SELECT * from " + TABLE_NAME + " where " + COL_ID + " = ? and " + COL_FROM_CARD_ID + " = ? ";
		Object[] objects = { userId, cardId };
		int[] types = { Types.INTEGER, Types.VARCHAR };
		return jdbcTemplate.query(sql, objects, types, new TransactionWrapper());
	}

}
