package com.learn.mavenspring.models.onecard;

public class CardDetails {

	private String cardId;
	private Integer userId;
	private Double amount;

	public CardDetails(String cardId, Integer userId) {

		this.cardId = cardId;
		this.userId = userId;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

}
