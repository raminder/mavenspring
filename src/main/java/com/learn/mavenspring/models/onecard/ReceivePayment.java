package com.learn.mavenspring.models.onecard;

public class ReceivePayment {

	private Integer userId;
	private String userMobileNumber;
	private Double amount;
	private String cardId;
	private String tranxId;
	private Integer fromUserId;
	private String fromMobileNumber;
	private String gcmMessageId;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserMobileNumber() {
		return userMobileNumber;
	}

	public void setUserMobileNumber(String userMobileNumber) {
		this.userMobileNumber = userMobileNumber;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getTranxId() {
		return tranxId;
	}

	public void setTranxId(String tranxId) {
		this.tranxId = tranxId;
	}

	public Integer getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(Integer fromUserId) {
		this.fromUserId = fromUserId;
	}

	public String getFromMobileNumber() {
		return fromMobileNumber;
	}

	public void setFromMobileNumber(String fromMobileNumber) {
		this.fromMobileNumber = fromMobileNumber;
	}

	public String getGcmMessageId() {
		return gcmMessageId;
	}

	public void setGcmMessageId(String gcmMessageId) {
		this.gcmMessageId = gcmMessageId;
	}

}
