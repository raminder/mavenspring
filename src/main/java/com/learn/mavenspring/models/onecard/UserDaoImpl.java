package com.learn.mavenspring.models.onecard;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.learn.mavenspring.mappers.UserMapper;
import com.mysql.jdbc.Statement;

public class UserDaoImpl implements UserDao {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		jdbcTemplate = new JdbcTemplate(dataSource);

	}

	public String create(User user) {

		try {
			String sql1 = "insert into " + TABLE_NAME + "( " + COL_FIRST_NAME + "," + COL_LAST_NAME + "," + COL_EMAIL
					+ "," + COL_PHONE_NUM + "," + COL_PASSWORD + "," + COL_GENDER + " , " + COL_DOB
					+ " ) values(?,?,?,?,?,?,?)";
			System.out.println("sql " + sql1);
			java.sql.PreparedStatement prepareStatement;
			prepareStatement = dataSource.getConnection().prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);
			prepareStatement.setString(1, user.get_firstName());
			prepareStatement.setString(2, user.get_lastName());
			prepareStatement.setString(3, user.get_email());
			prepareStatement.setString(4, user.get_phoneNumber());
			prepareStatement.setString(5, user.get_password());
			prepareStatement.setInt(6, user.get_gender());
			prepareStatement.setDate(7, user.get_dob());
			prepareStatement.executeUpdate();
			ResultSet rs = prepareStatement.getGeneratedKeys();
			int generatedId = 0;
			if (rs.next()) {
				generatedId = rs.getInt(1);
			}
			return "" + generatedId;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public boolean exists(User user) {

		String sql = " select count(*) from " + TABLE_NAME + "  where " + COL_EMAIL + " = ?";
		System.out.println("sql " + sql);
		Integer count = jdbcTemplate.queryForObject(sql, new String[] { user.get_email() }, Integer.class);
		if (count > 0) {
			return true;
		} else {
			return false;
		}

	}

	public Integer getId(String mobileNumber) {

		try {
			String sql = "select * from " + TABLE_NAME + " where " + COL_PHONE_NUM + " = ?";
			Object[] objects = { mobileNumber };
			int[] types = { Types.VARCHAR };
			User user = jdbcTemplate.queryForObject(sql, objects, types, new UserMapper());
			return user.get_userId();
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	public User exists(String mobileNumber) {

		try {
			String sql = "select * from " + TABLE_NAME + " where " + COL_PHONE_NUM + " = ?";
			Object[] objects = { mobileNumber };
			int[] types = { Types.VARCHAR };
			User user = jdbcTemplate.queryForObject(sql, objects, types, new UserMapper());
			return user;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	public User exists(Integer userId) {

		try {
			String sql = "select * from " + TABLE_NAME + " where " + COL_ID + " = ?";
			Object[] objects = { userId };
			int[] types = { Types.INTEGER };
			User user = jdbcTemplate.queryForObject(sql, objects, types, new UserMapper());
			return user;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	public boolean markVerified(String mobileNumber) {
		String updateSql = "UPDATE " + TABLE_NAME + " SET " + COL_NUM_VERIFIED + " = ? where " + COL_PHONE_NUM + " = ?";
		Object[] object = { 1, mobileNumber };
		int[] types = { Types.TINYINT, Types.VARCHAR };
		int cnt = jdbcTemplate.update(updateSql, object, types);
		if (cnt > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isVerified(String mobileNumber) {

		String sql = "select count(*) from " + TABLE_NAME + " where " + COL_PHONE_NUM + " = ? and " + COL_NUM_VERIFIED
				+ " = ?";

		Object[] objects = { mobileNumber, 1 };
		int[] types = { Types.VARBINARY, Types.TINYINT };
		int cnt = jdbcTemplate.queryForObject(sql, objects, types, Integer.class);
		if (cnt > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean creatMPIN(Integer userId, String mpin) {
		String updateSql = "UPDATE " + TABLE_NAME + " SET " + COL_MPIN + " =  ? where " + COL_ID + " = ?";
		Object[] objects = { mpin, userId };
		int types[] = { Types.VARCHAR, Types.VARCHAR };
		int cnt = jdbcTemplate.update(updateSql, objects, types);
		if (cnt > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean verifyMPIN(Integer userId, String mpin) {
		String sql = "select * from " + TABLE_NAME + " where " + COL_ID + " = ?";
		Object[] objects = { userId };
		int[] types = { Types.INTEGER };
		try {
			User user = jdbcTemplate.queryForObject(sql, objects, types, new UserMapper());
			if (mpin.equals(user.get_mpin())) {
				return true;
			} else {
				return false;
			}
		} catch (EmptyResultDataAccessException ex) {
			ex.printStackTrace();
			return false;
		}
	}

}
