package com.learn.mavenspring.models.onecard;

import java.util.List;

import javax.sql.DataSource;

public interface DocumentsDao {

	final String TABLE_NAME = "documents";
	final String COL_USER_ID = "id";
	final String COL_PANCARD = "pancard";
	final String COL_ADHARCARD = "adhar";
	final String COL_DRIVING_LIC = "driving";
	final String COL_VOTER_CARD = "votercard";

	void setDataSource(DataSource dataSource);

	boolean save(Document doc);

	boolean anyDocExists(Integer userId);

	List<FileType> fetchAll(Integer userId);
}
