package com.learn.mavenspring.models.onecard;

import java.io.Serializable;

public class PayRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Integer _userId;
	String _toMobileNumber;
	Double _amount;
	String _cardId;
	String _description;
	String _tranxId;

	public PayRequest() {

	}

	public Integer get_userId() {
		return _userId;
	}

	public void set_userId(Integer _userId) {
		this._userId = _userId;
	}

	public String get_toMobileNumber() {
		return _toMobileNumber;
	}

	public void set_toMobileNumber(String _toMobileNumber) {
		this._toMobileNumber = _toMobileNumber;
	}

	public Double get_amount() {
		return _amount;
	}

	public void set_amount(Double _amount) {
		this._amount = _amount;
	}

	public String get_cardId() {
		return _cardId;
	}

	public void set_cardId(String _cardId) {
		this._cardId = _cardId;

	}

	public String get_description() {
		return _description;
	}

	public void set_description(String _description) {
		this._description = _description;
	}

	public String get_tranxId() {
		return _tranxId;
	}

	public void set_tranxId(String _tranxId) {
		this._tranxId = _tranxId;
	}

}
