package com.learn.mavenspring.models.onecard;

import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.learn.mavenspring.Utils;
import com.learn.mavenspring.mappers.CardWrapper;

public class CardDaoImpl implements CardDao {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	public Card add(Integer userId, String accountNumber, String bankId) {

		String sql = "INSERT INTO " + TABLE_NAME + " ( " + COL_USER_ID + " , " + COL_ACCT_NUM + " , " + COL_CARD_ID
				+ " , " + COL_BANK_ID + " ) values (?,?,?,?)";

		System.out.println("add card" + sql);
		String cardId = Utils.getCardId(userId);
		Object[] objects = { userId, accountNumber, cardId, bankId };
		int types[] = { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };
		int cnt = this.jdbcTemplate.update(sql, objects, types);
		if (cnt > 0) {
			return new Card(cardId, 1000d);
		} else {
			return null;
		}

	}

	public Card getCard(Integer userId) {

		try {
			String sql = "select * from " + TABLE_NAME + " where " + COL_USER_ID + " = ?";
			Object[] objects = { userId };
			int[] types = { Types.INTEGER };
			return jdbcTemplate.queryForObject(sql, objects, types, new CardWrapper());
		} catch (EmptyResultDataAccessException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			return null;
		}
	}

	public Card getCard(String cardId) {

		try {
			String sql = "select * from " + TABLE_NAME + " where " + COL_CARD_ID + " = ?";
			Object[] objects = { cardId };
			int[] types = { Types.INTEGER };
			return jdbcTemplate.queryForObject(sql, objects, types, new CardWrapper());
		} catch (EmptyResultDataAccessException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			return null;
		}
	}

	public List<Card> getCards(Integer userId) {

		try {
			String sql = "select * from " + TABLE_NAME + " where " + COL_USER_ID + " = ?";
			Object[] objects = { userId };
			int[] types = { Types.INTEGER };
			return jdbcTemplate.query(sql, objects, types, new CardWrapper());
		} catch (EmptyResultDataAccessException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			return null;
		}
	}

	public boolean updateAmount(String cardId, double amount) {
		String updateSql = "UPDATE " + TABLE_NAME + " SET " + COL_AMOUNT + " = ? where " + COL_CARD_ID + " = ?";
		Object[] objects = { amount, cardId };
		int[] types = { Types.DOUBLE, Types.VARCHAR };
		int cnt = jdbcTemplate.update(updateSql, objects, types);
		if (cnt > 0) {
			return true;
		} else {
			return false;
		}
	}

}
