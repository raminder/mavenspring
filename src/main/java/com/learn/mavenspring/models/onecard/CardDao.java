package com.learn.mavenspring.models.onecard;

import java.util.List;

import javax.sql.DataSource;

public interface CardDao {

	public static final String TABLE_NAME = "cards";
	public static final String COL_CARD_ID = "card_id";
	public static final String COL_USER_ID = "id";
	public static final String COL_ACCT_NUM = "account_number";
	public static final String COL_BANK_ID = "bank_id";
	public static final String COL_AMOUNT = "amount";

	public void setDataSource(DataSource dataSource);

	public Card add(Integer userid, String accountNumber, String bankId);

	public boolean updateAmount(String cardId, double amount);

	public List<Card> getCards(Integer userId);

	public Card getCard(Integer userId);

	public Card getCard(String cardId);

	/*
	 * CREATE TABLE `onecard`.`cards` ( `card_id` VARCHAR(45) NOT NULL, `id` INT
	 * NOT NULL, `account_number` VARCHAR(45) NOT NULL, `bank_id` VARCHAR(45)
	 * NOT NULL, PRIMARY KEY (`card_id`));
	 */

}
