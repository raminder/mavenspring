package com.learn.mavenspring.models.onecard;

import javax.sql.DataSource;

public interface AddressDao {

	final String TABLE_NAME = "address";
	final String COL_ID = "id";
	final String COL_STREET = "street";
	final String COL_CITY = "city";
	final String COL_PINCODE = "pincode";

	void setDataSource(DataSource datasource);

	boolean create(Integer id, String steet, String city, String pincode);

	boolean update(Integer id, String street, String city, String pincode);

}
