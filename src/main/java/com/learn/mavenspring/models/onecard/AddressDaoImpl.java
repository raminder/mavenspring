package com.learn.mavenspring.models.onecard;

import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

public class AddressDaoImpl implements AddressDao {

	private DataSource datasource;
	private JdbcTemplate jdbbcTemplate;

	public void setDataSource(DataSource datasource) {
		this.datasource = datasource;
		jdbbcTemplate = new JdbcTemplate(this.datasource);

	}

	public boolean create(Integer id, String street, String city, String pincode) {
		String insertSql = " INSERT INTO " + TABLE_NAME + " ( " + COL_ID + ", " + COL_STREET + " , " + COL_CITY + " , "
				+ COL_PINCODE + " ) values (?,?,?,?)";
		System.out.println("create adress " + insertSql);
		Object[] objects = { id, street, city, pincode };
		int[] types = { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };
		jdbbcTemplate.update(insertSql, objects, types);
		return true;
	}

	public boolean update(Integer id, String street, String city, String pincode) {

		String updateSql = "UPDATE  " + TABLE_NAME + " SET " + COL_STREET + " = ? , " + COL_CITY + " = ? ,"
				+ COL_PINCODE + " = ? " + "WHERE " + COL_ID + "= ?";
		System.out.println(updateSql);

		Object[] objects = { street, city, pincode, id };
		int[] types = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER };
		int cnt = jdbbcTemplate.update(updateSql, objects, types);
		if (cnt == 0) {
			return false;
		} else {
			return true;
		}

	}

	public boolean exists(Integer id) {

		String sql = " select count(*) from " + TABLE_NAME + "  where " + COL_ID + " = ?";
		System.out.println("sql " + sql);
		Integer count = jdbbcTemplate.queryForObject(sql, new Integer[] { id }, Integer.class);
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

}
