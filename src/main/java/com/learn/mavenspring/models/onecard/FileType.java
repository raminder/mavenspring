package com.learn.mavenspring.models.onecard;

public enum FileType {

	PANCARD(DocumentsDao.COL_PANCARD), ADHAR_CARD(DocumentsDao.COL_ADHARCARD), DRIVING_LIC(
			DocumentsDao.COL_DRIVING_LIC), VOTER_CARD(DocumentsDao.COL_VOTER_CARD);

	private String value;

	FileType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static FileType getFileType(String value) {
		FileType[] values = FileType.values();
		for (FileType fileType : values) {

			if (fileType.getValue().equals(value)) {
				return fileType;
			}
		}
		return FileType.PANCARD;

	}
}
