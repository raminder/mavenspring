package com.learn.mavenspring.models.onecard;

import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.learn.mavenspring.mappers.SMSCodeWrapper;

public class SMSCodeDaoImpl implements SMSCodesDao {

	private DataSource datasource;
	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource datasource) {
		this.datasource = datasource;
		this.jdbcTemplate = new JdbcTemplate(datasource);

	}

	public boolean addCode(Integer id, String mobileNumber, String smsCode) {

		String insertSql = "INSERT INTO " + TABLE_NAME + " ( " + COL_ID + " , " + COL_MOBILE_NUM + " , " + COL_SMS_CODE
				+ ") values(?,?,?) ";
		Object objects[] = { id, mobileNumber, smsCode };
		int[] types = { Types.INTEGER, Types.VARCHAR, Types.VARCHAR };

		int cnt = jdbcTemplate.update(insertSql, objects, types);
		if (cnt > 0) {
			return true;
		}
		return false;

	}

	public boolean remove(int id, String mobileNumber) {
		return true;
	}

	public boolean isCodePresent(int id, String mobileNumber) {

		String sql = "select count(*) from  " + TABLE_NAME + " where " + COL_ID + "= ? " + " and " + COL_MOBILE_NUM
				+ " = ?";
		System.out.println("isCodePresent " + sql);
		int types[] = { Types.INTEGER, Types.VARCHAR };
		Object objects[] = { id, mobileNumber };
		int cnt = jdbcTemplate.queryForObject(sql, objects, types, Integer.class);
		if (cnt > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean updateCode(Integer id, String mobileNum, String smsCode) {

		String updateSql = " UPDATE " + TABLE_NAME + " SET " + COL_SMS_CODE + " =  ?  WHERE " + COL_ID + " = ? and "
				+ COL_MOBILE_NUM + "= ?";
		int[] types = { Types.VARCHAR, Types.INTEGER, Types.VARCHAR };
		Object[] objects = { smsCode, id, mobileNum };
		int cnt = jdbcTemplate.update(updateSql, objects, types);
		if (cnt > 0) {
			return true;
		} else {
			return false;
		}

	}

	public SMSCodeTuple getSMSTuple(String mobileNumber) {
		try {
			String sql = "select * from " + TABLE_NAME + " where " + COL_MOBILE_NUM + " = ?";
			Object[] objects = { mobileNumber };
			int[] types = { Types.VARCHAR };
			return jdbcTemplate.queryForObject(sql, objects, types, new SMSCodeWrapper());
		} catch (EmptyResultDataAccessException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			return null;
		}
	}

}
