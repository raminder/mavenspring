package com.learn.mavenspring.models.onecard;

import javax.sql.DataSource;

public interface ReceiveFromDao {

	public static final String TABLE_NAME = "receivefrom";
	public static final String COL_ID = "user_id";
	public static final String COL_MOBILE_NUM = "mobile_number";
	public static final String COL_AMOUNT = "amount";
	public static final String COL_IN_CARD_ID = "card_id";
	public static final String COL_TRANX_ID = "tranx_id";

	public static final String COL_FROM_USER_ID = "from_user_id";
	public static final String COL_FROM_MOBILE_NUMBER = "from_mobile_number";
	public static final String COL_STATUS = "status";
	public static final String COL_GCM_MESSAGE_ID = "gcm_message_id";

	void setDataSource(DataSource dataSource);

	boolean addRequest(ReceivePayment receivePayment);

	ReceivePayment fetchRequest(String tranxId);

}
