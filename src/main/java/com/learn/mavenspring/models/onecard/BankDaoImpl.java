package com.learn.mavenspring.models.onecard;

import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.learn.mavenspring.mappers.BankWrapper;

public class BankDaoImpl implements BankDao {

	private DataSource dataSource;
	private JdbcTemplate jdbc;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		jdbc = new JdbcTemplate(dataSource);

	}

	public List<Bank> getRegisteredBanks() {

		String sql = " select *  from " + TABLE_NAME + "  where " + COL_IS_REGISTERED + " = ?";
		Object[] objects = { 1 };
		int[] types = { Types.INTEGER };
		System.out.println("query " + sql);
		return jdbc.query(sql, objects, types, new BankWrapper());

	}

}
