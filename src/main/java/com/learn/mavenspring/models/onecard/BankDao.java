package com.learn.mavenspring.models.onecard;

import java.util.List;

import javax.sql.DataSource;

public interface BankDao {

	public static final String TABLE_NAME = "banks";

	public static final String COL_ORD_ID = "org_id";
	public static final String COL_HANDLE = "handle";
	public static final String COL_IS_REGISTERED = "is_registered";
	public static final String COL_NBIN = "nbin";
	public static final String COL_IFSC = "ifsc";
	public static final String COL_IIN = "iin";
	public static final String COL_NAME = "name";
	public static final String COL_LOGO_URL = "logo_url";

	void setDataSource(DataSource dataSource);

	List<Bank> getRegisteredBanks();

}
