package com.learn.mavenspring.models.onecard;

import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.learn.mavenspring.mappers.ReceivePaymentWrapper;

public class ReceiveFromDaoImpl implements ReceiveFromDao {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		jdbcTemplate = new JdbcTemplate(dataSource);

	}

	public ReceivePayment fetchRequest(String tranxId) {

		String sql = " select * from " + TABLE_NAME + " where " + COL_TRANX_ID + " = ?";
		Object[] objects = { tranxId };
		int[] types = { Types.VARCHAR };
		return jdbcTemplate.queryForObject(sql, objects, types, new ReceivePaymentWrapper());

	}

	public boolean addRequest(ReceivePayment receivePayment) {

		String insertSql = " INSERT " + TABLE_NAME + " ( " + COL_ID + " , " + COL_MOBILE_NUM + " , " + COL_TRANX_ID
				+ " , " + COL_IN_CARD_ID + " , " + COL_AMOUNT + " ,  " + COL_GCM_MESSAGE_ID + " , " + COL_FROM_USER_ID
				+ " , " + COL_FROM_MOBILE_NUMBER + " , " + COL_STATUS + " ) values (?,?,?,?,?,?,?,?,?) ";

		Object[] objects = { receivePayment.getUserId(), receivePayment.getUserMobileNumber(),
				receivePayment.getTranxId(), receivePayment.getCardId(), receivePayment.getAmount(),
				receivePayment.getGcmMessageId(), receivePayment.getFromUserId(), receivePayment.getFromMobileNumber(),
				0 };
		int[] types = { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.DOUBLE, Types.VARCHAR,
				Types.INTEGER, Types.VARCHAR, Types.TINYINT };
		int cnt = jdbcTemplate.update(insertSql, objects, types);
		if (cnt > 0) {
			return true;
		} else {
			return false;
		}
	}

}
