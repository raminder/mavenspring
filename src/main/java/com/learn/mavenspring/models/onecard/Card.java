package com.learn.mavenspring.models.onecard;

public class Card {

	private String cardId;
	private Double amount;
	
	public Card(String cardId, Double amount) {
		this.cardId = cardId;
		this.amount = amount;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
}
