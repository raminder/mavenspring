package com.learn.mavenspring.models.onecard;

public enum TranxType {
	DEBIT("debit"), CREDIT("credit");
	private String value;

	TranxType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static TranxType getEnum(String value) {
		for (TranxType tranxType : TranxType.values()) {
			if (tranxType.value.equals(value)) {
				return tranxType;
			}
		}
		return null;
	}

}