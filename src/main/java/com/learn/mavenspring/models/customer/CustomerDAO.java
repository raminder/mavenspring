package com.learn.mavenspring.models.customer;
public interface CustomerDAO 
{
	public void insert(CustomerInfo customer);
	public CustomerInfo findByCustomerId(int custId);
}