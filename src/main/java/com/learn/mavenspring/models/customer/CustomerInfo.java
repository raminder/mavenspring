package com.learn.mavenspring.models.customer;

public class CustomerInfo {

	String name;
	int customerId;
	int age;

	public CustomerInfo(int customerId, String name, int age) {
		super();
		this.name = name;
		this.customerId = customerId;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCustId() {
		return customerId;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name + " age " + age;
	}

	public void setCustId(int customerId) {
		this.customerId = customerId;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
