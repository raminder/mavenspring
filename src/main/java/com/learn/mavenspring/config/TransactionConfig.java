package com.learn.mavenspring.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import com.learn.mavenspring.models.onecard.TransactionDao;
import com.learn.mavenspring.models.onecard.TransactionDaoImpl;

@Configuration
@ImportResource("classpath:db/jdbc_dataSource.xml")
public class TransactionConfig {

	@Autowired
	DataSource dataSource;

	@Bean
	public TransactionDao getTranxBean() {
		TransactionDaoImpl impl = new TransactionDaoImpl();
		impl.setDataSource(dataSource);
		return impl;
	}

}
