package com.learn.mavenspring.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.annotation.MultipartConfig;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.learn.mavenspring.models.onecard.Document;
import com.learn.mavenspring.models.onecard.DocumentDaoImpl;
import com.learn.mavenspring.models.onecard.FileType;

@RestController
@RequestMapping(value = "/service")
// max size 20MB
@MultipartConfig(fileSizeThreshold = 20971520)
public class UploadService {

	@RequestMapping(value = "/{id}/upload/{type}")
	public ResponseEntity<String> uploadFile(@RequestParam("uploadFile") MultipartFile uploadedFileRef,
			@PathVariable("type") String fileType, @PathVariable Integer id) {

		// Get name of uploaded file.
		String fileName = FileType.getFileType(fileType).getValue() + "_" + id.toString();

		// Path where the uploaded file will be stored.
		String path = "C:\\Users\\RetailAdmin\\Documents\\" + fileName;

		boolean isFileSaved = saveFile(uploadedFileRef, fileName, path);
		if (isFileSaved) {
			ApplicationContext context = new ClassPathXmlApplicationContext("spring_module.xml");
			DocumentDaoImpl documentDaoImpl = (DocumentDaoImpl) context.getBean("documentDao");
			Document doc = new Document(FileType.getFileType(fileType), id, path);
			boolean savedToDb = documentDaoImpl.save(doc);
			if (savedToDb) {

				return new ResponseEntity<String>("File Uploaded Successfully", HttpStatus.CREATED);
			}
		}
		return new ResponseEntity<String>("File Uploaded Successfully", HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private boolean saveFile(MultipartFile uploadedFileRef, String fileName, String path) {
		// This buffer will store the data read from 'uploadedFileRef'
		byte[] buffer = new byte[1000];

		// Now create the output file on the server.
		File outputFile = new File(path);

		FileInputStream reader = null;
		FileOutputStream writer = null;
		int totalBytes = 0;
		try {

			outputFile.createNewFile();
			System.out.println("uploaded file name " + fileName + " size " + uploadedFileRef.getSize());
			// Create the input stream to uploaded file to read data from it.
			reader = (FileInputStream) uploadedFileRef.getInputStream();

			// Create writer for 'outputFile' to write data read from
			// 'uploadedFileRef'
			writer = new FileOutputStream(outputFile);

			// Iteratively read data from 'uploadedFileRef' and write to
			// 'outputFile';
			int bytesRead = 0;
			while ((bytesRead = reader.read(buffer)) != -1) {
				writer.write(buffer);
				totalBytes += bytesRead;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {

			try {
				reader.close();
				writer.close();

			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}

		return true;
	}
}
