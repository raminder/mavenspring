package com.learn.mavenspring.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.learn.mavenspring.Utils;
import com.learn.mavenspring.models.Status;
import com.learn.mavenspring.models.onecard.OTPVerification;
import com.learn.mavenspring.models.onecard.SMSCodeTuple;
import com.learn.mavenspring.models.onecard.SMSCodesDao;
import com.learn.mavenspring.models.onecard.User;
import com.learn.mavenspring.models.onecard.UserDao;
import com.learn.mavenspring.service.CheapBazarSmsGateway;
import com.learn.mavenspring.service.SMSGatewayService;

@Controller
@RequestMapping("/otp")
@ImportResource("classpath:spring_module.xml")
public class OTPController {

	@Autowired
	private SMSCodesDao smsCodeDaoImpl;

	@Autowired
	private UserDao userDao;

	@RequestMapping(value = "/generate", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Status> generateCode(@RequestParam("mobile_number") String mobileNumber) {

		Integer userId = userDao.getId(mobileNumber);
		String smsCode = "1234";// Utils.getRandomOTP();
		boolean status = false;
		if (userId != null) {
			// user already exists
			status = smsCodeDaoImpl.updateCode(userId, mobileNumber, smsCode);
		} else {
			String id = userDao.create(new User(null, null, null, mobileNumber, null, -1));
			status = smsCodeDaoImpl.addCode(Integer.parseInt(id), mobileNumber, smsCode);
		}

		System.out.println(smsCodeDaoImpl.toString());
		return new ResponseEntity<Status>(new Status(status), HttpStatus.OK);
	}

	@RequestMapping(value = "/verify", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<OTPVerification> verifyOTP(@RequestParam("mobile_number") String mobileNumber,
			@RequestParam("otp") String OTP) {

		// SMSCodesDao smsDao = (SMSCodeDaoImpl) context.getBean("smsCodeDao");
		SMSCodeTuple tuple = smsCodeDaoImpl.getSMSTuple(mobileNumber);

		OTPVerification otpVerification = new OTPVerification();

		if (tuple != null && tuple.getCode().equals(OTP)) {
			otpVerification.setUserId(tuple.getId());
			otpVerification.setMobileNumber(mobileNumber);
			userDao.markVerified(mobileNumber);
			otpVerification.setIsOTPVerified(true);

		} else {

			otpVerification.setIsOTPVerified(false);
		}
		return new ResponseEntity<OTPVerification>(otpVerification, HttpStatus.OK);

	}

	@RequestMapping("/test")
	public @ResponseBody String testing() {

		SMSGatewayService smsGateway = new CheapBazarSmsGateway();
		return String
				.valueOf(smsGateway.sendSMS("9911486522", "Welcome to NoCard. Your OTP is " + Utils.getRandomOTP()));

	}

	@RequestMapping("/isPresent")
	private @ResponseBody String isCodePresent() {

		boolean isPresent = smsCodeDaoImpl.isCodePresent(3, "9911486522");
		System.out.println(smsCodeDaoImpl.toString());
		return String.valueOf(isPresent);
	}

	@RequestMapping("/updateCode")
	private @ResponseBody String udpateCode() {

		boolean isPresent = smsCodeDaoImpl.updateCode(1, "9911486522", "4444");
		System.out.println(smsCodeDaoImpl.toString());
		return String.valueOf(isPresent);
	}

	@RequestMapping("/fetchEntry")
	private @ResponseBody SMSCodeTuple fetchCode() {

		SMSCodeTuple tuple = smsCodeDaoImpl.getSMSTuple("9911486522");
		return tuple;
	}

}
