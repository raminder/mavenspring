package com.learn.mavenspring.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.learn.mavenspring.Constants;
import com.learn.mavenspring.config.TransactionConfig;
import com.learn.mavenspring.gcm.Message;
import com.learn.mavenspring.gcm.Sender;
import com.learn.mavenspring.models.Status;
import com.learn.mavenspring.models.customer.CustomerDAO;
import com.learn.mavenspring.models.customer.CustomerInfo;
import com.learn.mavenspring.models.onecard.Address;
import com.learn.mavenspring.models.onecard.AddressDaoImpl;
import com.learn.mavenspring.models.onecard.Bank;
import com.learn.mavenspring.models.onecard.Card;
import com.learn.mavenspring.models.onecard.CardDetails;
import com.learn.mavenspring.models.onecard.CardDao;
import com.learn.mavenspring.models.onecard.CardDaoImpl;
import com.learn.mavenspring.models.onecard.Transaction;
import com.learn.mavenspring.models.onecard.TransactionDao;
import com.learn.mavenspring.models.onecard.User;
import com.learn.mavenspring.models.onecard.UserDaoImpl;
import com.learn.mavenspring.models.onecard.service.BankService;
import com.learn.mavenspring.models.questions.QuestionDBImpl;

//TODO Add Exception Handler for 500 and other DB Exceptions
//TODO Load JDBC and other resources proactively
//TODO create Authorization for timed logout
//TODO atomic transactions(all actual transactions will be happening at bank server)---flow of two simultaneous request  
//TODO Validations
//TODO Add device Id for each api call
//TODO Autowire

@Controller
public class TestController {

	@Autowired
	private BankService _bankService;

	@RequestMapping("/test")
	public @ResponseBody String test() {

		final Sender sender = new Sender(Constants.GCM_SERVER_KEY);
		final Message message = new Message.Builder().addData("user_id", "1")
				.addData("gcmDoneToday", "gcmWillbeDoneToday").build();
		final String to = "cP_-T1nBHwM:APA91bHPdsX414BsSSiEwpHo2Yd2ruvPt3QiEX74KjJ6THfLUMbGOO2tz-F4Cayubirj2FkpQo4P-5ZMbIXey7B9BY8OHXXyWzPDGsB5rRll4FYtTStc9tsu6eW1dbrykiW6bMJ-d7ON";

		Thread thread = new Thread(new Runnable() {

			public void run() {

				try {
					sender.send(message, to, 0);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		thread.start();
		return "Its working";
	}

	@RequestMapping("/testdb")
	public @ResponseBody String testDB() {

		ApplicationContext context = new ClassPathXmlApplicationContext("spring_module.xml");
		CustomerDAO customerDAO = (CustomerDAO) context.getBean("customerDAO");
		// Customer customer = new Customer(1, "raminder", 28);
		// customerDAO.insert(customer);

		CustomerInfo customer1 = customerDAO.findByCustomerId(1);
		System.out.println(customer1);
		return customer1.toString();
	}

	@RequestMapping("/putq")
	public @ResponseBody String testInsertQuestion() {

		ApplicationContext context = new ClassPathXmlApplicationContext("spring_module.xml");
		QuestionDBImpl questionsDBInterface = (QuestionDBImpl) context.getBean("questionsDBImpl");

		return "" + questionsDBInterface.insertQuestion();
	}

	@RequestMapping(value = "/user/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<User> createUser(@RequestBody User user) {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring_module.xml");
		UserDaoImpl userDaoImpl = (UserDaoImpl) context.getBean("userDao");
		// User user = new User("ramitest", "sahani", "email", "9911486522",
		// "password", 2);
		if (!userDaoImpl.exists(user)) {
			String id = userDaoImpl.create(user);
			user.set_userId(Integer.parseInt(id));
			return new ResponseEntity<User>(user, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<User>(HttpStatus.CONFLICT);
		}

	}

	@RequestMapping(value = "/user/isPresent")
	public @ResponseBody String isUserPresent(@RequestBody User user) {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring_module.xml");
		UserDaoImpl userDaoImpl = (UserDaoImpl) context.getBean("userDao");
		return String.valueOf(userDaoImpl.exists(user));
	}

	@RequestMapping(value = "/address/save")
	public @ResponseBody ResponseEntity<String> saveAddress(@RequestBody Address address) {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring_module.xml");
		AddressDaoImpl addressDaoImpl = (AddressDaoImpl) context.getBean("addressDao");

		boolean created = false;
		if (!addressDaoImpl.exists(address.getId())) {
			created = addressDaoImpl.create(address.getId(), address.getStreet(), address.getCity(),
					address.getPincode());
			return new ResponseEntity<String>(HttpStatus.CREATED);
		} else {
			created = addressDaoImpl.update(address.getId(), address.getStreet(), address.getCity(),
					address.getPincode());
			return new ResponseEntity<String>(HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/card/add", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<CardDetails> addCard(@RequestParam("user_id") String userid,
			@RequestParam("acc_no") String accountNumber, @RequestParam("bank_id") String bankId) {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring_module.xml");
		CardDaoImpl cardDaoImpl = (CardDaoImpl) context.getBean("carddao");
		Card card = cardDaoImpl.add(Integer.parseInt(userid), accountNumber, bankId);
		if (card != null) {
			CardDetails addResponse = convert(Integer.parseInt(userid), card);
			return new ResponseEntity<CardDetails>(addResponse, HttpStatus.OK);
		} else {
			return new ResponseEntity<CardDetails>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private CardDetails convert(Integer userid, Card card) {
		CardDetails addResponse = new CardDetails(card.getCardId(), userid);
		addResponse.setAmount(card.getAmount());
		return addResponse;
	}

	@RequestMapping(value = "/user/mpin/verify", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Status> verifyMPIN(@RequestParam("user_id") String userId,
			@RequestParam("mpin") String mpin) {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring_module.xml");
		UserDaoImpl userDaoImpl = (UserDaoImpl) context.getBean("userDao");
		Status status = new Status(userDaoImpl.verifyMPIN(Integer.parseInt(userId), mpin));
		return new ResponseEntity<Status>(status, HttpStatus.OK);
	}

	@RequestMapping(value = "/user/mpin/generate", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Status> generateMPIN(@RequestParam("user_id") String userId,
			@RequestParam("mpin") String mpin) {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring_module.xml");
		UserDaoImpl userDaoImpl = (UserDaoImpl) context.getBean("userDao");
		Status status = new Status(userDaoImpl.creatMPIN(Integer.parseInt(userId), mpin));
		return new ResponseEntity<Status>(status, HttpStatus.OK);
	}

	@RequestMapping(value = "/user/cards", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<List<CardDetails>> receive(@RequestParam("user_id") Integer userId) {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring_module.xml");
		CardDao cardDao = (CardDaoImpl) context.getBean("carddao");
		List<Card> cards = cardDao.getCards(userId);
		if (cards != null && cards.size() > 0) {
			List<CardDetails> cardAddResponseList = new ArrayList<CardDetails>(cards.size());
			for (int i = 0; i < cards.size(); i++) {
				cardAddResponseList.add(convert(userId, cards.get(i)));
			}
			return new ResponseEntity<List<CardDetails>>(cardAddResponseList, HttpStatus.OK);
		} else {
			return new ResponseEntity<List<CardDetails>>(new ArrayList<CardDetails>(), HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/user/tranx", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<List<Transaction>> transaction(@RequestParam("user_id") Integer userId,
			@RequestParam(value = "card_id", required = false) String cardId) {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(TransactionConfig.class);

		TransactionDao transactionDao = ctx.getBean(TransactionDao.class);
		List<Transaction> transactions = null;
		if (cardId == null) {
			transactions = transactionDao.getAll(userId);
		} else {
			transactions = transactionDao.getAll(userId, cardId);
		}
		if (transactions != null && transactions.size() > 0) {
			return new ResponseEntity<List<Transaction>>(transactions, HttpStatus.OK);
		} else {
			return new ResponseEntity<List<Transaction>>(new ArrayList<Transaction>(), HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/banks", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Bank>> bankList(@RequestParam("user_id") Integer userId) {
		List<Bank> bankList = _bankService.getRegisteredBankList();
		if (bankList != null) {
			return new ResponseEntity<List<Bank>>(bankList, HttpStatus.OK);
		} else {
			return new ResponseEntity<List<Bank>>(new ArrayList<Bank>(), HttpStatus.OK);
		}
	}
}
