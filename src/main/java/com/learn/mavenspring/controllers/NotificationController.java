package com.learn.mavenspring.controllers;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.learn.mavenspring.models.Status;
import com.learn.mavenspring.models.onecard.NotificationRegisterDao;
import com.learn.mavenspring.models.onecard.NotificationRegisterDaoImpl;
import com.learn.mavenspring.models.onecard.NotificationRegisterRequest;

@Controller
@RequestMapping("/notif")
public class NotificationController {

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Status> register(
			@RequestBody NotificationRegisterRequest notifRegisterRegquest) {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring_module.xml");
		NotificationRegisterDao notifDao = (NotificationRegisterDaoImpl) context.getBean("notificationRegisterDao");
		Status status = new Status(false);
		if (notifDao.register(notifRegisterRegquest)) {
			status.setStatus(true);
		}
		return new ResponseEntity<Status>(status, HttpStatus.OK);
	}

	@RequestMapping(value = "/deregister", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Status> deregister(
			@RequestBody NotificationRegisterRequest notifDeRegisterRequest) {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring_module.xml");
		NotificationRegisterDao notifDao = (NotificationRegisterDaoImpl) context.getBean("notificationRegisterDao");
		Status status = new Status(false);
		if (notifDao.deregister(notifDeRegisterRequest)) {
			status.setStatus(true);
		}
		return new ResponseEntity<Status>(status, HttpStatus.OK);
	}

}
