package com.learn.mavenspring.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.learn.mavenspring.Constants;
import com.learn.mavenspring.Utils;
import com.learn.mavenspring.config.TransactionConfig;
import com.learn.mavenspring.gcm.Message;
import com.learn.mavenspring.gcm.Message.Builder;
import com.learn.mavenspring.gcm.Result;
import com.learn.mavenspring.gcm.Sender;
import com.learn.mavenspring.models.Status;
import com.learn.mavenspring.models.onecard.Card;
import com.learn.mavenspring.models.onecard.CardDao;
import com.learn.mavenspring.models.onecard.NotificationRegisterDao;
import com.learn.mavenspring.models.onecard.PayRequest;
import com.learn.mavenspring.models.onecard.ReceiveFromDao;
import com.learn.mavenspring.models.onecard.ReceivePayment;
import com.learn.mavenspring.models.onecard.TransactionDao;
import com.learn.mavenspring.models.onecard.TranxType;
import com.learn.mavenspring.models.onecard.User;
import com.learn.mavenspring.models.onecard.UserDao;

@Controller
@ImportResource("classpath:spring_module.xml")
public class PaymentController {

	@Autowired
	private CardDao cardDaoImpl;
	@Autowired
	private UserDao userDaoImpl;
	@Autowired
	private ReceiveFromDao receiveFromdao;
	@Autowired
	private NotificationRegisterDao notifDao;

	@RequestMapping(value = "/user/pay", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Status> pay(@RequestBody PayRequest payRequest) {
		Status status = new Status(false);
		if (payRequest.get_tranxId() == null) {
			Card fromDefaultCard = cardDaoImpl.getCard(payRequest.get_cardId());
			status = handlePayRequest(payRequest, fromDefaultCard);
		} else {
			ReceivePayment receivePaymentRequest = receiveFromdao.fetchRequest(payRequest.get_tranxId());
			boolean isValid = validate(receivePaymentRequest, payRequest);
			if (isValid) {
				Card toCard = cardDaoImpl.getCard(receivePaymentRequest.getCardId());
				Card fromCard = cardDaoImpl.getCard(payRequest.get_cardId());
				if (payRequest.get_amount() > 0 && payRequest.get_amount() <= fromCard.getAmount()) {
					Double fromCardBalance = fromCard.getAmount() - payRequest.get_amount();
					Double toCardBalance = toCard.getAmount() + payRequest.get_amount();
					cardDaoImpl.updateAmount(fromCard.getCardId(), fromCardBalance);
					cardDaoImpl.updateAmount(toCard.getCardId(), toCardBalance);
					status.setStatus(true);
				}
			}
		}

		return new ResponseEntity<Status>(status, HttpStatus.OK);
	}

	private boolean validate(ReceivePayment receivePaymentRequest, PayRequest payRequest) {
		boolean hasValidTranxId = receivePaymentRequest.getTranxId().equals(payRequest.get_tranxId());
		boolean hasValidAmount = receivePaymentRequest.getAmount().equals(payRequest.get_amount());
		boolean isValidAmount = receivePaymentRequest.getFromUserId().equals(payRequest.get_userId());
		return hasValidTranxId && hasValidAmount && isValidAmount;
	}

	private Status handlePayRequest(PayRequest payRequest, Card fromDefaultCard) {
		Integer toUserId = userDaoImpl.getId(payRequest.get_toMobileNumber());
		Card toDefaultCard = cardDaoImpl.getCard(toUserId);
		Status status = new Status(false);
		if (fromDefaultCard != null && toDefaultCard != null && fromDefaultCard.getAmount() > payRequest.get_amount()) {
			System.out.println("Has valid Card " + fromDefaultCard.getAmount());
			fromDefaultCard.setAmount(fromDefaultCard.getAmount() - payRequest.get_amount());
			toDefaultCard.setAmount(toDefaultCard.getAmount() + payRequest.get_amount());

			cardDaoImpl.updateAmount(fromDefaultCard.getCardId(), fromDefaultCard.getAmount());
			cardDaoImpl.updateAmount(toDefaultCard.getCardId(), toDefaultCard.getAmount());
			AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(TransactionConfig.class);

			TransactionDao payToDao = ctx.getBean(TransactionDao.class);
			status.setStatus(payToDao.pay(payRequest, TranxType.DEBIT, Utils.getRandomTranxId()));
		}
		return status;
	}

	@RequestMapping(value = "/user/receive", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Status> receive(@RequestBody PayRequest payRequest) {

		User fromUser = userDaoImpl.exists(payRequest.get_toMobileNumber());
		User user = userDaoImpl.exists(payRequest.get_userId());
		if (fromUser != null) {
			String userGcmId = notifDao.getGcmId(fromUser.get_userId());
			String tranxId = Utils.getRandomTranxId();
			String messageId = sendNotification(fromUser.get_userId(), payRequest, tranxId, user.get_phoneNumber(),
					userGcmId);

			ReceivePayment receivePayment = getReceiveRequest(payRequest, user, fromUser, tranxId, messageId);
			boolean success = receiveFromdao.addRequest(receivePayment);
			Status status = new Status(success);
			return new ResponseEntity<Status>(status, HttpStatus.OK);
		} else {
			System.out.println("mobile number " + payRequest.get_toMobileNumber() + " is not on our platform");
			return new ResponseEntity<Status>(new Status(false), HttpStatus.OK);
		}
	}

	private ReceivePayment getReceiveRequest(PayRequest payRequest, User user, User requestFromUser, String tranxId,
			String messageId) {

		ReceivePayment receivePayment = new ReceivePayment();
		receivePayment.setUserId(user.get_userId());
		receivePayment.setUserMobileNumber(user.get_phoneNumber());
		receivePayment.setAmount(payRequest.get_amount());
		receivePayment.setTranxId(tranxId);
		receivePayment.setGcmMessageId(messageId);
		receivePayment.setCardId(payRequest.get_cardId());

		receivePayment.setFromMobileNumber(requestFromUser.get_phoneNumber());
		receivePayment.setFromUserId(requestFromUser.get_userId());
		return receivePayment;
	}

	private String sendNotification(Integer toUserId, PayRequest payRequest, String tranxId, String userMobileNumber,
			String toGcmId) {
		Sender sender = new Sender(Constants.GCM_SERVER_KEY);
		Builder builder = new Message.Builder();
		builder.addData("type", "101");
		builder.addData("user_id", "" + toUserId);
		builder.addData("amount", "" + payRequest.get_amount());
		builder.addData("tranxId", tranxId);
		builder.addData("mobile_number", userMobileNumber);
		builder.addData("message", payRequest.get_description());
		Result result = null;
		try {
			result = sender.send(builder.build(), toGcmId, 1);
			if (result.getMessageId() != null) {
				String updatedGcmId = result.getCanonicalRegistrationId();
				// TODO update cannonical id
				return result.getMessageId();
			} else {
				String errorCode = result.getErrorCodeName();
				System.out.println("error code from gcm " + errorCode);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
