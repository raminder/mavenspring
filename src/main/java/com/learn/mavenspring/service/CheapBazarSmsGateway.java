package com.learn.mavenspring.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learn.mavenspring.NetworkUtils;

public class CheapBazarSmsGateway implements SMSGatewayService {

	final String SEND_SMS_URL = "http://login.cheapsmsbazaar.com/vendorsms/pushsms.aspx?user=raminder&password=trextech";
	// &msisdn=919898 xxxxxx&sid=SenderId&msg=test%20 message&fl=0

	public boolean sendSMS(String mobileNumber, String smsBody) {
		String urlString = SEND_SMS_URL + "&msisdn=" + mobileNumber + "&sid=trextech&fl=0";
		String encodedMessage = "";
		try {
			encodedMessage = URLEncoder.encode(smsBody, "UTF-8");
			System.out.println("encoded" + encodedMessage);
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		urlString = urlString + "&msg=" + encodedMessage;
		System.out.println("final url" + urlString);
		String response = NetworkUtils.sendGetRequest(urlString);
		System.out.println("response from cheap server" + response);
		ObjectMapper mapper = new ObjectMapper();
		boolean status = false;
		try {
			JsonNode rootNode = mapper.readTree(response);
			String errorMessage = rootNode.get("ErrorMessage").asText();
			System.out.println("message from cheap server" + errorMessage + " full response " + rootNode.asText());
			if (errorMessage.toLowerCase().equals("success")) {
				status = true;
			}

		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return status;
	}

}
