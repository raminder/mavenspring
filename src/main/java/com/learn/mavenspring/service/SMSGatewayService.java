package com.learn.mavenspring.service;

public interface SMSGatewayService {

	public boolean sendSMS(String mobileNumber, String smsBody);
}
