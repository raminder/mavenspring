package com.learn.mavenspring;

import java.util.Date;
import java.util.Random;

public class Utils {

	public static Object getCurrentDate() {
		Date date = new Date();
		Object param = new java.sql.Timestamp(date.getTime());
		return param;
	}

	public static String getRandomOTP() {

		int max = 9999;
		int min = 1000;
		Random random = new Random();
		int randomNum = random.nextInt((max - min) + 1) + min;

		return String.valueOf(randomNum);
	}

	public static String getCardId(Integer userId) {
		return getRandomOTP() + "" + String.valueOf(userId);
	}

	public static String getRandomTranxId() {

		Random random = new Random();
		long randomNum = random.nextLong();
		long randomTranxId = new Date().getTime() + randomNum;
		return String.valueOf(randomTranxId);
	}

}
